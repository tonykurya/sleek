## Deploy the Cloud Endpoints

``$ gcloud endpoints services deploy openapi.yaml``

## Get Authentication Credentials

``$ gcloud container clusters get-credentials cl-cluster --zone us-central1-f``

## Assign yourself the cluster-admin role

``$ kubectl create clusterrolebinding cluster-admin-binding \
--clusterrole cluster-admin --user $(gcloud config get-value account)``

## Deploy web app to cluster

``$ kubectl apply -f configmap.yaml``

``$ kubectl apply -f deployment.yaml``

## Expose the deployment by attaching a NodePort service

``$ kubectl apply -f service.yaml``

# Set up Let's Encrypt

``$ helm install --name cert-manager --version v0.3.2 \
    --namespace kube-system stable/cert-manager``
    
``$ export EMAIL=levihai49@gmail.com``

``$ cat letsencrypt-issuer.yaml | sed -e "s/email: ''/email: $EMAIL/g" | kubectl apply -f-``

## Configure ingress for HTTPS

``$ kubectl apply -f ingress-tls.yaml``

## Check the status

``$ kubectl describe ingress esp-ingress``

#### Note: It might take a few minutes for the ingress to be properly provisioned.

# Test the application by connecting to the address below

https://sleekcrm.com




